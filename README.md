NIR Viewer
=========

NIR Viewer is a simple tool to view logs from `NIR_PRINT=1`

### Features

##### Syntax Coloring and Line Numbering

![syntax_coloring](./doc/syntax_coloring.jpg)

##### Shaders Grouped by Types

![types](./doc/types.jpg)

##### Passes Listed in Foldable Views

* each pass is initially folded, with its name and line count printed
* clicking on `▷` will unfold it and display the NIR code

![passes_fold.jpg](./doc/passes_fold.jpg)

##### SSA values highlighting

Clicking on a ssa value will highlight it on every line where it's used.

![ssa_highlight.jpg](./doc/ssa_highlight.jpg)

##### Consecutive Passes Differences Indicators

* `+` = this line has been added by the viewed pass
* `-` = this line will be removed by the next pass

![pass_delta.jpg](./doc/pass_delta.jpg)

##### Opening Multiple Files and Comparing Them

* files are compared per pass
* to select which passes are compared: unfold a pass and either double-click on it or click the `Compare` button at the top
* each open file has at most one active `Compare` pass
* N files can be opened. The comparison is always done to the file to the right (except for the right-most file with is compared to the left-most one)
* the comparison ignores all `ssa_XX` values in the lines

![file_compare.jpg](./doc/file_compare.jpg)

##### Fast Loading

* 3.5 sec for a 3.3 GB log file with 1600+ shaders
* loading is threaded so the app is usable while loading is in progress


### License

MIT License (see [LICENSE](./LICENSE))

This project uses [Dear ImGui](https://github.com/ocornut/imgui) which is also licensed under the MIT License.